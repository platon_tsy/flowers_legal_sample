/**
 * Created by aleksey on 26.08.17.
 */
export interface PolicyInfoTitle {
    shippingPolicy: string;
    claimsPolicy: string;
    paymentPolicy: string;
}
