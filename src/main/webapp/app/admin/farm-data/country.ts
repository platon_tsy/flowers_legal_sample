/**
 * Created by andreysinya on 21.06.17.
 */
export interface Country {
    text: string;
    id: string;
}
