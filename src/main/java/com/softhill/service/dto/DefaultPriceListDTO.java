package com.softhill.service.dto;

import com.softhill.domain.Market;

public class DefaultPriceListDTO extends PriceListDTO {

    private Market market;

    public DefaultPriceListDTO() {
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }
}
