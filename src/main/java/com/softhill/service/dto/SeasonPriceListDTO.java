package com.softhill.service.dto;

import com.softhill.domain.MarketSeason;

public class SeasonPriceListDTO extends PriceListDTO {

    private MarketSeason marketSeason;

    public SeasonPriceListDTO() {
    }

    public MarketSeason getMarketSeason() {
        return marketSeason;
    }

    public void setMarketSeason(MarketSeason marketSeason) {
        this.marketSeason = marketSeason;
    }
}
