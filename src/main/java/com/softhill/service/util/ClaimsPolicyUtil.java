package com.softhill.service.util;

import com.softhill.domain.ClaimsPolicy;
import com.softhill.domain.Company;

import java.util.Arrays;
import java.util.List;

public class ClaimsPolicyUtil {
    /**
     *
     * @param company
     * @return List<ClaimsPolicy>
     */
    public static List<ClaimsPolicy> getDefaultClaimsPolicyForCompany(Company company) {
        return Arrays.asList(
            new ClaimsPolicy().shortName("FOB").fullName("Free On Board").company(company)

        );
    }
}
