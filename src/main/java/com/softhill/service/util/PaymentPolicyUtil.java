package com.softhill.service.util;

import com.softhill.domain.Company;
import com.softhill.domain.PaymentPolicy;

import java.util.Arrays;
import java.util.List;

public class PaymentPolicyUtil {

    /**
     *
     * @param company
     * @return List<PaymentPolicy>
     */
    public static List<PaymentPolicy> getDefaultPaymentPolicyForCompany(Company company) {
        return Arrays.asList(
            new PaymentPolicy().name("Prepay").company(company),
            new PaymentPolicy().name("Credit").company(company)
        );
    }
}
