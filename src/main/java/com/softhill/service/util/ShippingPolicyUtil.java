package com.softhill.service.util;

import com.softhill.domain.Company;
import com.softhill.domain.ShippingPolicy;

import java.util.Arrays;
import java.util.List;

public class ShippingPolicyUtil {
    /**
     *
     * @param company
     * @return List<ShippingPolicy>
     */
    public static List<ShippingPolicy> getDefaultShippingPolicyForCompany(Company company) {
    return Arrays.asList(
        new ShippingPolicy().shortName("FOB").fullName("Free On Board").company(company)

    );
}
}
