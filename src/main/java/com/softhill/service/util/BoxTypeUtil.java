package com.softhill.service.util;

import com.softhill.domain.BoxType;
import com.softhill.domain.Company;
import com.softhill.domain.MarketBox;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by aleksey on 04.08.17.
 */
public class BoxTypeUtil {

    /**
     * Get Default List of BoxTypes for Company
     * @param company
     * @return List<BoxType>
     */
    public static List<BoxType> getDefaultBoxTypes4Company(Company company) {
        return Arrays.asList(
            new BoxType().shortName("QB").fullName("Quarter Box").company(company),
            new BoxType().shortName("HB").fullName("Half Box").company(company),
            new BoxType().shortName("FB").fullName("Full Box").company(company)
        );
    }
}
