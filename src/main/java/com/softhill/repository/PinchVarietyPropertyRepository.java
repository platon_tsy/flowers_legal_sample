package com.softhill.repository;

import com.softhill.domain.PinchVarietyProperty;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the PinchVarietyProperty entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PinchVarietyPropertyRepository extends JpaRepository<PinchVarietyProperty,Long> {

}
