package com.softhill.domain.enumeration;

/**
 * The PriceListType enumeration.
 */
public enum PriceListType {
    DEFAULT,HIGH,LOW,SEASON
}
