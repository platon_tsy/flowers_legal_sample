package com.softhill.domain.enumeration;

/**
 * The BoxSizeUnit enumeration.
 */
public enum BoxSizeUnit {
    MM,M,CM
}
