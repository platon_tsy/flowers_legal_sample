/**
 * View Models used by Spring MVC REST controllers.
 */
package com.softhill.web.rest.vm;
