package com.softhill.web.rest.request;

/**
 * Created by platon on 20.06.17.
 */
public class PasswordChangeRequest {
    private String newPassword;
    private String oldPassword;
    private String login;

    public PasswordChangeRequest() {
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
