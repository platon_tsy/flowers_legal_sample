package com.softhill.web.rest.util;

import com.softhill.domain.ClaimsPolicy;
import com.softhill.domain.ShippingPolicy;

/**
 * Created by oleg on 28.07.17.
 */
public class ClaimsPolicyUtils {

    public static ShippingPolicy convertClaimToShipping (ClaimsPolicy claimsPolicy) {
        ShippingPolicy shippingPolicyResult = new ShippingPolicy();
        shippingPolicyResult.setShortName(claimsPolicy.getShortName());
        shippingPolicyResult.setFullName(claimsPolicy.getFullName());
        shippingPolicyResult.setId(claimsPolicy.getId() != null ? claimsPolicy.getId() : null);

        return shippingPolicyResult;
    }
}
