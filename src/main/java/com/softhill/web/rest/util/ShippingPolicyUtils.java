package com.softhill.web.rest.util;

import com.softhill.domain.ClaimsPolicy;
import com.softhill.domain.ShippingPolicy;

/**
 * Created by oleg on 28.07.17.
 */
public class ShippingPolicyUtils {

    public static ClaimsPolicy convertShippingToClaim (ShippingPolicy shippingPolicy) {
        ClaimsPolicy claimsPolicyResult = new ClaimsPolicy();
        claimsPolicyResult.setShortName(shippingPolicy.getShortName());
        claimsPolicyResult.setFullName(shippingPolicy.getFullName());
        claimsPolicyResult.setId(shippingPolicy.getId() != null ? shippingPolicy.getId() : null);

        return claimsPolicyResult;
    }
}
