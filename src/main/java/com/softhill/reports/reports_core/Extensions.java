package com.softhill.reports.reports_core;

/**
 * Created by aleksey on 10.05.17.
 */
public class Extensions {

    private static final String pdf = ".pdf";

    public static String getPdf() {
        return pdf;
    }
}
