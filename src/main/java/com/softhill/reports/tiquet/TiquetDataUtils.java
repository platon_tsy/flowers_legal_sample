package com.softhill.reports.tiquet;


/**
 * @author Aleksey Yelizarenko
 */
public class TiquetDataUtils {

	private Tiquet data;
	private Integer boxId;


	public Tiquet getData() {
		return data;
	}

	public void setData(Tiquet data) {
		this.data = data;
	}

	public Integer getBoxId() {
		return boxId;
	}

	public void setBoxId(Integer boxId) {
		this.boxId = boxId;
	}

}
