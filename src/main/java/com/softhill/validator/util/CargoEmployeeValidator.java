package com.softhill.validator.util;

import com.softhill.domain.CargoEmployee;

public class CargoEmployeeValidator {
    public static boolean checkLengthValidation(CargoEmployee cargoEmployee) {
        if(cargoEmployee.getFullName().length() > 50 || cargoEmployee.getEmail().length() > 50 || cargoEmployee.getSkype().length() > 50) {
            return true;
        }
        return false;
    }
}
