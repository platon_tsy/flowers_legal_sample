package com.softhill.validator.util;

import com.softhill.domain.CargoEmployeePosition;

public class CargoEmployeePositionValidator {
    public static boolean checkLengthValidation(CargoEmployeePosition cargoEmployeePosition) {
        if(cargoEmployeePosition.getName().length() > 25) {
            return true;
        }
        return false;
    }
}
