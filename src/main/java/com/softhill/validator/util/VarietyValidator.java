package com.softhill.validator.util;

import com.softhill.domain.Variety;

/**
 * Created by aleksey on 31.08.17.
 */
public class VarietyValidator {


    public static boolean checkLengthValidation(Variety variety) {
        if(variety.getName().length() > 35 || variety.getColor().length() > 25
        || (variety.getBreeder() == null || variety.getBreeder().length() > 25)
        || (variety.getMinLength() == null || variety.getMinLength().toString().length() > 4)
        || (variety.getMaxLength() == null || variety.getMaxLength().toString().length() > 4)) {
            return false;
        }
        return true;
    }
}
