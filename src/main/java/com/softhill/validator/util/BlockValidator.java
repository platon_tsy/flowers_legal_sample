package com.softhill.validator.util;

import com.softhill.domain.Block;

public class BlockValidator {

	public static boolean checkName(Block block) {
		if (block != null && !block.getName().isEmpty()) {
			return block.getName().length() < 11;
		}
		return false;
	}

	public static boolean checkBeds(Block block) {
		if (block != null && block.getBeds() != 0) {
			return block.getBeds() < 99999;
		}
		return false;
	}

}
