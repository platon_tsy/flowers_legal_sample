package com.softhill.validator.util;

import com.softhill.domain.BoxType;

/**
 * Created by aleksey on 03.08.17.
 */
public class BoxTypeValidator {

    public static boolean checkLengthValidation(BoxType boxType) {
        return (boxType.getShortName().length() > 5 || boxType.getFullName().length() > 13);
    }
}
