package com.softhill.validator.util;

import com.softhill.domain.AirLines;
import com.softhill.service.AirLinesServiceSH;
import com.softhill.web.rest.util.HeaderUtil;
import org.springframework.http.ResponseEntity;

/**
 * Created by andrey on 02.08.17.
 */
public class AirLinesValidator {

    public static boolean checkID(AirLines airLines) {
        return (airLines.getId() != null);
    }

    public  static boolean checkFieldsLength(AirLines airLines) {
        return ((airLines.getName().length() <= 25) && (airLines.getNumber().toString().length() <=5));
    }

    public static boolean AirLinesIdentityCheck(AirLines airLines, AirLinesServiceSH airLinesServiceSH) {
        for (AirLines airLinesFromList : airLinesServiceSH.findAirLinesByCompanyId()) {
            if (airLinesFromList.getName().equals(airLines.getName())) {
                return true;
            }
        }
        return false;
    }
}
