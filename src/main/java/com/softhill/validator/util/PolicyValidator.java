package com.softhill.validator.util;

import com.softhill.domain.ClaimsPolicy;
import com.softhill.domain.ShippingPolicy;

public class PolicyValidator {

    public static boolean checkLengthValidationShippingPolicy(ShippingPolicy shippingPolicy) {
        if(shippingPolicy.getShortName().length() > 17 ||shippingPolicy.getFullName().length() > 25)  {
            return false;
        }
        return false;
    }

    public static boolean checkLengthValidationClaimsPolicy(ClaimsPolicy claimsPolicy) {
        if(claimsPolicy.getShortName().length() > 17 ||claimsPolicy.getFullName().length() > 25)  {
            return false;
        }
        return false;
    }
}
